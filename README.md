# Chat with Pusher API

[View demo](https://chat.savayer.space)

### Installation

create .env file with your app keys, then run commands:

```sh
$ composer install
```

and if you need 

```sh
$ npm i
```